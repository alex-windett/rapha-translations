require 'httparty'
require 'pry'
require 'pry-byebug'

$languageFiles = {}
$lang_prefixs = []

def getLanguageKeys(row)
  first_row = row["records"][0]["fields"]

  first_row.each do |item|
    $lang_prefixs << item[0]
  end

  return $lang_prefixs
end

def createLanguageFileRow(lang, record)
  translation = record["fields"][lang]
  key = record["fields"]["Key"]
  
  return { key: key, translation: translation }
end

def createEmptyFiles(languageKeys)
  languageKeys.each do |lang|
    $languageFiles[lang] = {}
  end
end

def createFiles(fileObject) 
  $lang_prefixs.each do |lang|

    prefix = lang.downcase

    File.open("./locales/#{prefix}.translations.js", "a") do |file|

      index = 1

      $languageFiles[lang].each_key do |translation_key|
        
        file_line_content = %Q("#{translation_key}": "#{$languageFiles[lang][translation_key]}",)
        file_line = "#{file_line_content} \n"

        if index == 1
          file_line = "{ \n #{file_line_content} \n"
        end

        if index == $languageFiles[lang].length
          file_line = "#{file_line_content} \n} "
        end

        index = index + 1

        file << file_line
      end
    end
  end
end


Dir.glob('./locales/*.translations.js').each do |file| 

  if ( File.file?(file) )
    File.delete(file)
  end
end


url = 'https://api.airtable.com/v0/apph3LeJ36NioLiUT/Page%20Translations'
headers = {
  Authorization: "Bearer #{ARGV[0]} "
}

response = HTTParty.get(url, headers: headers)

languageKeys = getLanguageKeys(response)
languageKeys.delete_at(languageKeys.find_index('Key'))

createEmptyFiles(languageKeys)

languageKeys.each do |lang|
  response["records"].each do |record| 
    file_row = createLanguageFileRow(lang, record)

    $languageFiles[lang][file_row[:key]] = file_row[:translation]
  end
end

createFiles($languageFiles)

puts response.body